// currently hardcoded to the background image size
// pub const WORLD_SIZE: f32 = 3000.;
pub const DELTA_TIME: f32 = 0.01;
pub const SERVER_SLEEP_DURATION: u64 = 10;

pub const WINDOW_SIZE: f32 = 700.;

pub const MENU_BACKGROUND_COLOR: (u8, u8, u8) = (108, 57, 57);

pub const NAME_POS: (f32, f32) = (50., 150.);

pub const GRAVITY_SWITCH_INTERVAL: f32 = 3.;

// Revolutions per second
pub const CAMERA_ROTATION_SPEED: f32 = 4.;

// number of tiles per dimension
pub const MAP_SIZE: usize = 32;
pub const TILE_SIZE: usize = 32;

pub const BACKGROUND_TILE_ID: usize = 0;

pub const TILEMAP_PATH: &str = "../resources/map.txt";
pub const GRAVITY_ACCELERATION: f32 = 0.01;

pub const SHOT_SCALE_STRENGTH: f32 = 10.;
pub const MAX_SHOOT_PROGRESS: f32 = 100.;
