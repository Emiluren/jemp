use serde_derive::{Serialize, Deserialize};

use crate::math::{Vec2, vec2};
use crate::tilemap;
use crate::constants;

const PLAYER_FORWARD_INERTIA: f32 = 10.0;

const JUMP_STRENGTH: f32 = 500.0;
const SHOOT_PROGRESS_STEP: f32 = 2.5;

const BOUNCE_ELASTICITY: f32 = 1.0;

#[derive(Serialize, Deserialize, Clone)]
pub struct Player {
    pub id: u64,
    pub name: String,

    pub input_x: f32,
    pub jump_input: bool,
    pub already_jumping: bool,
    pub shoot_input: bool,
    pub rotate_input: bool,
    pub already_shooting: bool,

    pub shoot_progress: f32,

    pub position: Vec2,
    pub velocity: Vec2,

    pub mouse_position: (f32, f32),
}


impl Player {
    pub fn new(
        id: u64,
        name: String
    ) -> Player {
        Player {
            id,
            name,

            input_x: 0.,
            jump_input: false,
            already_jumping: false,
            shoot_input: false,
            rotate_input: false,

            position: vec2(200., 200.),
            shoot_progress: 0.,
            already_shooting: false,

            velocity: vec2(0., 0.),
            mouse_position: (0., 0.),
        }
    }

    pub fn set_input(&mut self, input_x: f32, jump_input: bool, shoot_input: bool, mouse_position: (f32, f32)) {
        self.input_x = input_x;
        self.shoot_input = shoot_input;
        self.jump_input = jump_input;
        self.mouse_position = mouse_position;
    }

    pub fn update(&mut self, delta_time: f32, gravity: Vec2, tilemap: &tilemap::TileMap) {
        // self.velocity += gravity.orthogonal_ccw().normalize() * (self.input_x * PLAYER_FORWARD_INERTIA);
        self.velocity.x += self.input_x * PLAYER_FORWARD_INERTIA;
        println!("{}, {}", self.position.x, self.position.y);
        self.velocity += gravity;

        // Shoot
        if self.shoot_progress == 0. {
            if self.shoot_input && !self.already_shooting {
                self.shoot_progress += SHOOT_PROGRESS_STEP;
                self.already_shooting = true;
            }
        } else {
            if self.shoot_input && self.shoot_progress != constants::MAX_SHOOT_PROGRESS {
                self.shoot_progress += SHOOT_PROGRESS_STEP;
                self.already_shooting = true;
            }
        }
        if self.shoot_input == false {
            self.already_shooting = false;
        }

        // Jump
        if self.jump_input && !self.already_jumping {
            // self.velocity += gravity.normalize() * -JUMP_STRENGTH;
            self.velocity.y = -JUMP_STRENGTH;
            self.already_jumping = true;
        } else if !self.jump_input {
            self.already_jumping = false;
        }

        self.position += self.velocity * delta_time;
        // let collision_normal = tilemap.collides_with(self.position, new_position);
        // match collision_normal {
        //     None => self.position = new_position,
        //     Some(normal) => {
        //         let dot_product =
        //             self.velocity.x * normal.x + self.velocity.y * normal.y;
        //         let reflection = vec2(self.velocity.x - BOUNCE_ELASTICITY * dot_product * normal.x,
        //             self.velocity.y - BOUNCE_ELASTICITY * dot_product * normal.y);
        //         self.velocity = reflection;
        //     }
        // }

    }
}
