use std::sync::mpsc::Receiver;

use serde_derive::{Serialize, Deserialize};

use crate::player::Player;
use crate::math::{Vec2, vec2};
use crate::tilemap;
use crate::constants;
use crate::projectile::Projectile;

#[derive(Serialize, Deserialize, Clone)]
pub struct GameState {
    pub players: Vec<Player>,
    pub projectiles: Vec<Projectile>,
    pub gravity: Vec2,
}

impl GameState {
    pub fn new() -> GameState {
        GameState {
            players: Vec::new(),
            projectiles: Vec::new(),
            gravity: Vec2{ x: 0.0, y: constants::GRAVITY_ACCELERATION},
        }
    }

    pub fn update(&mut self, tilemap: &tilemap::TileMap, delta: f32) {
        for player in &mut self.players {
            // println!("{} {}", player.position.x, player.position.y);
            player.update(delta, self.gravity, tilemap);
            // println!("{} {}", player.position.x, player.position.y);

            // Shoot if shoot progress has reached 100 or if player releases SPACE
            if player.shoot_progress == 100.0 || player.shoot_progress != 0. && !player.shoot_input {
                let velocity_magnitude = ((player.mouse_position.0 - player.position.x).powi(2) + 
                                               (player.mouse_position.1 - player.position.y).powi(2))
                                               .sqrt() / constants::SHOT_SCALE_STRENGTH;
                let projectile_velocity = vec2(player.shoot_progress * (player.mouse_position.0 - player.position.x) / velocity_magnitude,
                                                     player.shoot_progress * (player.mouse_position.1 - player.position.y) / velocity_magnitude);
                let projectile = Projectile::new(player.position, player.velocity + projectile_velocity);
                self.projectiles.push(projectile.clone());
                player.shoot_progress = 0.;
            }
        }

        for projectile in &mut self.projectiles {
            projectile.update(delta, self.gravity);
        }

    }

    pub fn add_player(&mut self, player: Player) {
        self.players.push(player.clone());
    }

    pub fn get_player_by_id(&self, id: u64) -> Option<&Player> {
        for player in &self.players {
            if player.id == id {
                return Some(player);
            }
        }
        None
    }

    // pub fn switch_gravity(&mut self, angle: f32) {
    //     self.gravity =  Vec2{ x: angle.cos() * constants::GRAVITY_ACCELERATION, y: angle.sin() * constants::GRAVITY_ACCELERATION};
    // }
}
