use crate::constants;
use serde_derive::{Deserialize, Serialize};
use std::ops::{Add, AddAssign, Div, Mul, Neg, Sub};

#[derive(Serialize, Deserialize, Clone, Copy, Debug)]
pub struct Vec2 {
    pub x: f32,
    pub y: f32,
}

pub fn vec2(x: f32, y: f32) -> Vec2 {
    Vec2 { x, y }
}

fn ccw(A: Vec2, B: Vec2, C: Vec2) -> bool {
    (C.y-A.y) * (B.x-A.x) >= (B.y-A.y) * (C.x-A.x)
}

// Return true if line segments AB and CD intersect
fn intersect(A: Vec2, B: Vec2, C: Vec2, D: Vec2) -> bool {
    ccw(A, C, D) != ccw(B, C, D) && ccw(A, B, C) != ccw(A, B, D)
}


pub fn intersects(l1: (Vec2, Vec2), l2: (Vec2, Vec2)) -> bool {
    let (l1_start, l1_end) = l1;
    let (l2_start, l2_end) = l2;
    intersect(l1_start, l1_end, l2_start, l2_end)
}

impl Add for Vec2 {
    type Output = Vec2;

    fn add(self, other: Vec2) -> Self {
        Vec2 {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl AddAssign for Vec2 {
    fn add_assign(&mut self, other: Vec2) {
        *self = *self + other;
    }
}

impl Sub for Vec2 {
    type Output = Vec2;

    fn sub(self, other: Vec2) -> Self {
        Vec2 {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl Neg for Vec2 {
    type Output = Vec2;

    fn neg(self) -> Self {
        Vec2 {
            x: -self.x,
            y: -self.y,
        }
    }
}

impl Mul<f32> for Vec2 {
    type Output = Vec2;

    fn mul(self, scalar: f32) -> Self {
        Vec2 {
            x: self.x * scalar,
            y: self.y * scalar,
        }
    }
}

impl Div<f32> for Vec2 {
    type Output = Vec2;

    fn div(self, scalar: f32) -> Self {
        Vec2 {
            x: self.x / scalar,
            y: self.y / scalar,
        }
    }
}

impl Vec2 {
    pub fn from_direction(angle: f32, length: f32) -> Self {
        Vec2 {
            x: angle.cos() * length,
            y: angle.sin() * length,
        }
    }

    pub fn norm(self) -> f32 {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }

    pub fn normalize(self) -> Vec2 {
        self / self.norm()
    }

    pub fn angle(self) -> f32 {
        self.y.atan2(self.x)
    }

    pub fn distance_to(self, other: Self) -> f32 {
        (self - other).norm()
    }

    pub fn dot(self, other: Self) -> f32 {
        self.x * other.x + self.y * other.y
    }

    // pub fn orthogonal_ccw(self) -> Self {
    //     Self {
    //         x: self.y,
    //         y: -self.x,
    //     }
    // }
}

pub fn modulo(x: f32, div: f32) -> f32 {
    (x % div + div) % div
}

pub fn angle_diff(source_angle: f32, target_angle: f32) -> f32 {
    // From https://stackoverflow.com/a/7869457
    use std::f32::consts::PI;
    modulo(target_angle - source_angle + PI, 2. * PI) - PI
}
