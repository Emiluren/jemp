﻿use serde_derive::{Serialize, Deserialize};
use crate::math::Vec2;

#[derive(Serialize, Deserialize, Clone)]
pub struct Projectile {
    pub position: Vec2,
    pub velocity: Vec2,
}

impl Projectile {
    pub fn new(
        position: Vec2,
        velocity: Vec2,
    ) -> Projectile {
        Projectile {
            position,
            velocity,
        }
    }

    pub fn update(&mut self, delta_time: f32, gravity: Vec2) {
        self.velocity += gravity;
        self.position += self.velocity * delta_time;
    }
}