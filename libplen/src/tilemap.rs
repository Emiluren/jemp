use std::fs;
use crate::constants;
use crate::math::{vec2, Vec2, intersects};

pub struct TileMap {
    pub grid: [[usize; constants::MAP_SIZE]; constants::MAP_SIZE],
}

impl TileMap {
    pub fn new(path: &str) -> TileMap {
        println!("{}", path);
        let contents = fs::read_to_string(path).expect("Can't read tilemap!");
        let lines = contents.lines().map(|l| l.chars().collect::<Vec<_>>()).collect::<Vec<_>>();
        let chars = contents.chars().collect::<Vec<_>>();
        let mut grid = [[0; constants::MAP_SIZE]; constants::MAP_SIZE];
        for i in 0..constants::MAP_SIZE {
            for j in 0..constants::MAP_SIZE {
                let c = lines[i][j];//chars[i * constants::MAP_SIZE + j];
                let num: usize = c.to_string().parse().unwrap();
                grid[i][j] = num;
            }
        }
        TileMap {
            grid
        }
    }

    pub fn get_tile_index(&self, row: usize, col: usize) -> usize {
        self.grid[row][col]
    }

    // pub fn collides_with(&self, old_pos: Vec2, new_pos: Vec2) -> Option<Vec2> {
        // let total_size = (constants::TILE_SIZE * constants::MAP_SIZE) as f32;
        // if new_pos.x < 0.0 || new_pos.x >= total_size || new_pos.y < 0.0 || new_pos.y >= total_size {
        //     return None;
        // }

        // let row = ((new_pos.y / (constants::TILE_SIZE as f32)).floor() as usize);
        // let col = ((new_pos.x / (constants::TILE_SIZE as f32)).floor() as usize);
        // if self.get_tile_index(row, col) == constants::BACKGROUND_TILE_ID {
        //     return None;
        // }
        // let left_x = (col * constants::TILE_SIZE) as f32;
        // let right_x = ((col + 1) * constants::TILE_SIZE - 1) as f32;
        // let top_y = (row * constants::TILE_SIZE) as f32;
        // let bottom_y = ((row + 1) * constants::TILE_SIZE - 1) as f32;

        // let top = (vec2(left_x, top_y), vec2(right_x, top_y));
        // let left = (vec2(left_x, top_y), vec2(left_x, bottom_y));
        // let bottom = (vec2(left_x, bottom_y), vec2(right_x, bottom_y));
        // let right = (vec2(right_x, top_y), vec2(right_x, bottom_y));

        // let path = (old_pos, new_pos);

        // // top
        // if intersects(path, top) {
        //     Some(vec2(0.0, 1.0))
        // } else if intersects(path, bottom) {
        //     Some(vec2(0.0, -1.0))
        // } else if intersects(path, left) {
        //     Some(vec2(1.0, 0.0))
        // } else if intersects(path, right) {
        //     Some(vec2(-1.0, 0.0))
        // } else {
        //     None
        // }
    // }
}
