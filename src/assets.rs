use enum_map::{enum_map, EnumMap};

use macroquad::texture::*;
use libplen::constants;
use libplen::tilemap;

pub struct Assets {
    pub malcolm: Texture2D,
    pub tilemap: tilemap::TileMap,
    pub tileset: Vec<Texture2D>,
    pub projectile: Texture2D,
}

impl Assets {
    pub fn new() -> Assets {
        let mut assets = Assets {
            malcolm: Texture2D::from_file_with_format(include_bytes!("../resources/malcolm.png"), None),
            tilemap: tilemap::TileMap::new("../resources/map.txt"),
            tileset: vec![
                Texture2D::from_file_with_format(include_bytes!("../resources/tiles/background.png"), None),
                Texture2D::from_file_with_format(include_bytes!("../resources/tiles/platform.png"), None),
            ],
            projectile: Texture2D::from_file_with_format(include_bytes!("../resources/malcolm.png"), None),
        };
        assets
    }
}
