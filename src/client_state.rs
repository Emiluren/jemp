use libplen::constants::{self, CAMERA_ROTATION_SPEED};
use libplen::gamestate::GameState;
use libplen::math::{self, vec2, Vec2};
use macroquad::prelude::*;
use macroquad::texture;

use crate::assets::Assets;

pub struct ClientState {
    camera_angle: f32,
}

impl ClientState {
    pub fn new() -> ClientState {
        ClientState {
            // init client stuff
            camera_angle: std::f32::consts::PI,
        }
    }

    pub fn update(&mut self, delta_time: f32, game_state: &GameState, my_id: u64) {
        // update client side stuff

        // self.compute_camera_angle(game_state, delta_time)
    }

    // pub fn compute_camera_angle(&mut self, game_state: &GameState, delta_time: f32) {
    //     let pi = std::f32::consts::PI;
    //     let graivty_angle = game_state.gravity.angle() + pi/2.;

    //     // Stolen :) https://math.stackexchange.com/questions/2144234/interpolating-between-2-angles
    //     let shortest_angle = ((((graivty_angle - self.camera_angle) % (pi*2.)) + pi*3.) % (pi*2.)) - pi;
    //     self.camera_angle = if shortest_angle != 0. {
    //         let angle_direction = shortest_angle / shortest_angle.abs();
    //         let angle_move_amount = angle_direction * CAMERA_ROTATION_SPEED * delta_time;

    //         if angle_move_amount.abs() > shortest_angle.abs() {
    //             graivty_angle
    //         }
    //         else {
    //             self.camera_angle + angle_move_amount
    //         }
    //     }
    //     else {
    //         self.camera_angle
    //     }
    // }

    fn draw_tilemap(&self, assets: &Assets) {
        for row in 0..constants::MAP_SIZE {
            for col in 0..constants::MAP_SIZE {
                let tile_index = assets.tilemap.get_tile_index(row, col);
                let tile_texture = assets.tileset[tile_index];
                texture::draw_texture(tile_texture,
                    (col * constants::TILE_SIZE) as f32, (row * constants::TILE_SIZE) as f32, WHITE);
            }
        }
    }

    pub fn draw(
        &self,
        my_id: u64,
        game_state: &GameState,
        assets: &mut Assets,
    ) -> Result<(), String> {

        clear_background(BLACK);

        // set_camera(&Camera2D {
        //     zoom: macroquad::prelude::vec2(1./screen_width(), 1./screen_height()),
        //     target: macroquad::prelude::vec2(screen_width(), screen_height())/2.,
        //     rotation: self.camera_angle / std::f32::consts::PI * 180.,
        //     ..Default::default()
        // });
        self.draw_tilemap(assets);

        // clear_background(BLACK);
        for player in &game_state.players {
            let params = texture::DrawTextureParams {
                dest_size: None,
                source: None,
                rotation: 0.0,
                flip_x: false,
                flip_y: false,
                pivot: None,
            };

            let px = player.position.x;
            let py = player.position.y;

            texture::draw_texture_ex(assets.malcolm, px, py, BLUE, params);

            const PROGRESS_BAR_DIMENSION: (f32, f32) = (25., 5.); // (width, height)
            const PROGRESS_BAR_OFFSET: (f32, f32) = (0., -5.);
            // Background progress bar
            draw_rectangle(player.position.x + PROGRESS_BAR_OFFSET.0, 
                           player.position.y + PROGRESS_BAR_OFFSET.1, 
                           PROGRESS_BAR_DIMENSION.0, 
                           PROGRESS_BAR_DIMENSION.1, 
                           RED);
            // Loading progress bar
            draw_rectangle(player.position.x + PROGRESS_BAR_OFFSET.0, 
                           player.position.y + PROGRESS_BAR_OFFSET.1, 
                           PROGRESS_BAR_DIMENSION.0 * (player.shoot_progress / constants::MAX_SHOOT_PROGRESS), 
                           PROGRESS_BAR_DIMENSION.1, 
                           GREEN);
        }

        for projectile in &game_state.projectiles {
            let params = texture::DrawTextureParams {
                dest_size: None,
                source: None,
                rotation: 0.0,
                flip_x: false,
                flip_y: false,
                pivot: None,
            };

            let px = projectile.position.x;
            let py = projectile.position.y;

            texture::draw_texture_ex(assets.projectile, px, py, RED, params);
        }

        // Render UI after this
        set_default_camera();

        Ok(())
    }
}
